terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "myapp-bucket-jl"
    key = "myapp/state.tfstate"
    region = "us-west-2"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.20.0"
    }
  }
}
